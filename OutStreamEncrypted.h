#pragma once

#include "OutStream.h"
class OutStreamEncrypted : public OutStream
{
private:
	int _hist;
public:
	OutStreamEncrypted(int heist);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char *str);
};

