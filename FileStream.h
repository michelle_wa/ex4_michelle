#pragma once
#include "OutStream.h"

class FileStream : public OutStream
{
public:
	FileStream(const char* path);
	~FileStream();
	FileStream& operator<<(const char *str);
	FileStream& operator<<(int num);
	FileStream& operator<<(void(*pf)());
	void endline();

};
