#include <iostream>
#include "OutStream.h"
#include "FileStream.h"
#include <string>
#define _CRT_SECURE_NO_WARNINGS


/*c'tor*/
FileStream::FileStream(const char* path)
{
	_pFile = fopen(path, "w");
}

/*d'tor*/
FileStream::~FileStream()
{
	fclose(_pFile);
}

/*prints to file*/
FileStream& FileStream::operator<<(const char *str)
{
	fprintf(_pFile, "%s", str);
	return *this;
}
FileStream& FileStream::operator<<(int num)
{
	fprintf(_pFile, "%d", num);
	return *this;
}
FileStream& FileStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}
void FileStream::endline()
{
	fprintf(this->_pFile, "%s", "\n");
}
